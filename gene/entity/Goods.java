package entity;

import java.io.Serializable;

/**
 * (Goods)实体类
 *
 * @author makejava
 * @since 2024-12-02 20:16:42
 */
public class Goods implements Serializable {
    private static final long serialVersionUID = -69395735278200122L;
    /**
     * ID
     */
    private Integer id;
    /**
     * 图片
     */
    private String preview;
    /**
     * 介绍
     */
    private String desc;
    /**
     * 价格
     */
    private Double price;
    /**
     * 类目
     */
    private String type;
    /**
     * 订单状态(1未付款,2已付款,3配送中,4已完成)
     */
    private Integer status;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}

