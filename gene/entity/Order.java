package entity;

import java.util.Date;
import java.io.Serializable;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2024-12-02 20:16:30
 */
public class Order implements Serializable {
    private static final long serialVersionUID = -63173164538089817L;
    /**
     * ID
     */
    private Integer id;
    /**
     * 总价
     */
    private Double price;
    /**
     * 收货信息
     */
    private String info;
    /**
     * 支付方式
     */
    private String pay;
    /**
     * 下单用户
     */
    private String user;
    /**
     * 下单时间
     */
    private Date time;
    /**
     * 订单状态(1未付款,2已付款,3配送中,4已完成)
     */
    private Integer status;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}

