setlocal enabledelayedexpansion

set MYAPPNAME=cake
set WEBDIR=.\testwebpage
set TOMCAT_HOME=C:\apache-tomcat-9.0.97
set JAVA_HOME=C:\Program Files\Java\jdk-17
::set TOMCAT_HOME=C:\Users\zhecai\Desktop\apache-tomcat-9.0.97
::set JAVA_HOME=C:\Program Files\Eclipse Adoptium\jdk-21.0.3.9-hotspot

@echo cleaning old files
@del /q /f /s %TOMCAT_HOME%\webapps\%MYAPPNAME%\* 1>nul
@del /q /f /s target 1>nul
@echo cleand

@echo complie ing...  
dir /s /b src\*.java > sources.txt
"%JAVA_HOME%\bin\javac" -cp lib\* -d "target\%MYAPPNAME%\WEB-INF\classes" @sources.txt -encoding utf-8
@echo complied
@echo 部署中
mkdir target\%MYAPPNAME%\WEB-INF\lib
copy lib\* target\%MYAPPNAME%\WEB-INF\lib\*
xcopy /s /y %WEBDIR%\* target\%MYAPPNAME% 1>nul
xcopy /s /y .\target\%MYAPPNAME%\* %TOMCAT_HOME%\webapps\%MYAPPNAME%\ 1>nul
@echo 部署完成
@echo run!!!! run it your self