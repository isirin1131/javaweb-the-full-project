<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>登录</title>
    <link href="static/bootstrap-3.4.1/css/bootstrap.css" rel="stylesheet">
    <link href="static/nav.css" rel="stylesheet">
</head>
<body>

<%--导航栏--%>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">蛋糕商城</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="actives"><a href="login.jsp">登录</a></li>
                <li><a href="register.jsp">注册</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><i class="glyphicon glyphicon-search"></i></a></li>
                <li><a href="#"><i class="glyphicon glyphicon-shopping-cart"></i> 0</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<%--登录表单--%>
<div class="container">

    <c:if test="${errorMsg != null}">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>错误！</strong> ${errorMsg}
        </div>
    </c:if>

    <div class="panel panel-info">
        <div class="panel-heading">用户登录</div>
        <div class="panel-body">
            <form class="form-horizontal" action="LoginServlet" method="post">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">用户</label>
                    <div class="col-sm-10">
                        <input type="text" name="user" class="form-control" id="inputEmail3" placeholder="用户名">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">密码</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="密码">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">登录</button>
                        <button type="reset" class="btn btn-warning">重置</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <p>没有账户？<a href="register.jsp">立即注册</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="static/bootstrap-3.4.1/js/jquery-1.12.4.js"></script>
<script src="static/bootstrap-3.4.1/js/bootstrap.js"></script>

</body>
</html>
