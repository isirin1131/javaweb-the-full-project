<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>个人中心</title>
    <link href="static/bootstrap-3.4.1/css/bootstrap.css" rel="stylesheet">
    <link href="static/nav.css" rel="stylesheet">
</head>
<body>

<%--导航栏--%>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">蛋糕商城</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="actives"><a href="register.jsp">个人中心</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="LoginServlet">登出</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<div class="container">

    <!-- 收货信息 -->
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">收货信息</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">收货姓名</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="${user.name}" placeholder="姓名">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">收获电话</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="${user.phone}" placeholder="电话">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">收获地址</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="${user.address}" placeholder="地址">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">提交</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <hr/>

    <!-- 安全信息 -->
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">安全信息</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">原密码</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputEmail3" placeholder="原密码">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">新秘密</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword3" placeholder="新密码">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">提交</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
