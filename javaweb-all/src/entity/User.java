package entity;

public class User {
    // ID
    private long id;
    // 用户名
    private String user;
    // 密码
    private String password;
    // 邮箱
    private String email;
    // 姓名
    private String name;
    // 电话
    private String phone;
    // 地址
    private String address;

    public User() {
    }

    public User(String user, String password, String email, String name, String phone, String address) {
        this.user = user;
        this.password = password;
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public User(long id, String user, String password, String email, String name, String phone, String address) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setDdress(String ddress) {
        this.address = ddress;
    }
}
