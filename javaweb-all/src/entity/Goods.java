package entity;

public class Goods {
    private long id;
    private String preview;
    private String desc;
    private double price;
    private String type;
    private long status;

    public Goods() {
    }

    public Goods(String preview, String desc, double price, String type, long status) {
        this.preview = preview;
        this.desc = desc;
        this.price = price;
        this.type = type;
        this.status = status;
    }

    public Goods(long id, String preview, String desc, double price, String type, long status) {
        this.id = id;
        this.preview = preview;
        this.desc = desc;
        this.price = price;
        this.type = type;
        this.status = status;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPreview() {
        return this.preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getStatus() {
        return this.status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

}
