package entity;

public class Order {
    private long id;
    private double price;
    private String info;
    private String pay;
    private String user;
    private String time;
    private long status;

    public Order() {
    }

    public Order(double price, String info, String pay, String user, String time, long status) {
        this.price = price;
        this.info = info;
        this.pay = pay;
        this.user = user;
        this.time = time;
        this.status = status;
    }

    public Order(long id, double price, String info, String pay, String user, String time, long status) {
        this.id = id;
        this.price = price;
        this.info = info;
        this.pay = pay;
        this.user = user;
        this.time = time;
        this.status = status;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getInfo() {
        return this.info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPay() {
        return this.pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getStatus() {
        return this.status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

}
