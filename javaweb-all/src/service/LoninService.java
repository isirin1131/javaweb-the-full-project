package service;

import dao.LoginDao;
import entity.User;

public class LoninService {
    private LoginDao loginDao = new LoginDao();


    /**
     * 登录功能
     *
     * @params: [user 用户名, password 密码]
     * @return: entity.User 登录用户对象
     */
    public User login(String user, String password) {
        User login = loginDao.login(user, password);
        return login;
    }
}
