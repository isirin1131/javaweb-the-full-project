package service;

import dao.RegisterDao;
import entity.User;

public class RegisterService {
    private RegisterDao registerDao = new RegisterDao();

    /**
     * 新增用户
     *
     * @params: [user 新增对象]
     * @return: boolean（true:成功，fase:失败）
     */
    public boolean insert(User user) {
        return registerDao.insert(user);
    }
}
